<?php




use Carbon_Fields\Field;
use Carbon_Fields\Container;
//use Carbon_Fields\Block;



add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
    Container::make( 'theme_options', __( 'Theme Options' ) )
        ->add_fields( array(
            Field::make( 'rich_text', 'crb_footer_copyright', 'Copyright' ),
        ) );


}

use Carbon_Fields\Block;

add_action( 'carbon_fields_register_fields', 'makers' );
function makers() {
    
    Container::make( 'post_meta', 'makers' )
    ->where( 'post_type', '=', 'post' )
    
    ->add_fields( array(
        Field::make( 'map', 'crb_location' )->set_visible_in_rest_api( $visible = true )
            ->set_position( 37.423156, -122.084917, 14 ),
        Field::make( 'sidebar', 'crb_custom_sidebar' )->set_visible_in_rest_api( $visible = true ),
        Field::make( 'image', 'crb_photo' )->set_visible_in_rest_api( $visible = true ),
    ));

   
}






//Gutemberg example
// Block::make( __( 'My Shiny Gutenberg Block' ) )
// ->add_fields( array(
//     Field::make( 'text', 'heading', __( 'Block Heading' ) ),
//     Field::make( 'image', 'image', __( 'Block Image' ) ),
//     Field::make( 'rich_text', 'content', __( 'Block Content' ) ),
// ) )
// ->set_description( __( 'A simple block consisting of a heading, an image and a text content.' ) )
// ->set_render_callback( function () {
//     // ..
// } );


?>



