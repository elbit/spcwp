var postcss = require('gulp-postcss');
var gulp = require('gulp');
var rename = require('gulp-rename');
const {watch}  = require('gulp');
var tailwindcss = require('tailwindcss');

// var scss = require(gulp-scss)
// sass.compiler = require('node-sass');

//var autoprefixer = require('autoprefixer');
//var browserSync = require('browser-sync').create();


function css(cb) {
  return gulp.src('tailwind.css')
    // ...
    .pipe(postcss([
      // ...
      tailwindcss('tailwind.config.js')
      //require('autoprefixer'),
      // ...
    ]))
    // ...
    .pipe(rename('tailwind-out.css'))
    .pipe(gulp.dest('../build'));
  cb();
}


function sass(cb) {
  return gulp.src('../elbit-fw.scss')
  .pipe(rename('elbit-fw.scss'))
  .pipe(gulp.dest('../build'));
   cb();

}

exports.sass = function() { 
  return this()
}

exports.css = function() { 
  return css()
}


exports.default = function() {
  // You can use a single task
  watch('tailwind.css', css);
  watch('tailwind.css', sass);
  // Or a composed task
  //watch('src/*.js', series(clean, javascript));
};

