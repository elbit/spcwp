<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package spcWP
 */

?>
	<footer id="colophon" class="">
		<div class="">
			<a href="">Contacto</a>
			<a href=""> Politica de privacidad</a>
		</div>
	</footer>


<?php wp_footer(); ?>


</body>
</html>
